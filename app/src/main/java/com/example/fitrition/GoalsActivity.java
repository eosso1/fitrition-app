package com.example.fitrition;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class GoalsActivity extends AppCompatActivity {

    private Button workoutGoalButton;
    private Button healthGoalButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goals);

        healthGoalButton = (Button) findViewById(R.id.HealthGoalButton);
        healthGoalButton.setOnClickListener(new View.OnClickListener() {
            Intent intent = new Intent(GoalsActivity.this, HealthGoalsActivity.class);

            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.HealthGoalButton:
                        startActivity(intent);
                        break;
                }
            }
        });

        workoutGoalButton = (Button) findViewById(R.id.WorkoutGoalButton);
        workoutGoalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GoalsActivity.this, WorkoutGoalsActivity.class);
                switch (view.getId()) {
                    case R.id.WorkoutGoalButton:
                        startActivity(intent);
                        break;
                }
            }
        });
    }
}
