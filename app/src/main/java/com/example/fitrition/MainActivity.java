package com.example.fitrition;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button goalButton;
    private Button tcButton;
    private Button workoutButton;
    private Button achButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        goalButton = (Button) findViewById(R.id.GoalsButton);
        goalButton.setOnClickListener(new View.OnClickListener() {
            Intent intent = new Intent(MainActivity.this, GoalsActivity.class);
            @Override
            public void onClick(View view) {
                switch(view.getId()) {
                    case R.id.GoalsButton:
                        startActivity(intent);
                        break;
                }
            }
        });

        tcButton = (Button) findViewById(R.id.TCButton);
        tcButton.setOnClickListener(new View.OnClickListener() {
            Intent intent = new Intent(MainActivity.this, TrackCaloriesActivity.class);
            @Override
            public void onClick(View view) {
                switch(view.getId()) {
                    case R.id.TCButton:
                        startActivity(intent);
                        break;
                }
            }
        });

        workoutButton = (Button) findViewById(R.id.WorkoutButton);
        workoutButton.setOnClickListener(new View.OnClickListener() {
            Intent intent = new Intent(MainActivity.this, WorkoutsActivity.class);
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.WorkoutButton:
                        startActivity(intent);
                        break;
                }
            }
        });

        achButton = (Button) findViewById(R.id.AchievementButton);
        achButton.setOnClickListener(new View.OnClickListener() {
            Intent intent = new Intent(MainActivity.this, AchievementsActivity.class);
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.AchievementButton:
                        startActivity(intent);
                        break;
                }
            }
        });
    }
}
